<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Main</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/admin') }}">Admin</a>
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Main
        </div>

        <form action="/" method="post" id="feedback">
            <div class="form-group">
                <label for="name">Your name</label>
                <input type="text" class="form-control" placeholder="Enter your name" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="phone">Your phone</label>
                <input class="form-control" type="tel" placeholder="Enter your phone" id="phone" name="phone" required>
            </div>
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" placeholder="Enter your email" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="text">Your message</label>
                <textarea class="form-control" rows="3" placeholder="Enter your message" id="text" name="message" required></textarea>
            </div>
            <div class="alert alert-secondary" id="message" role="alert">

            </div>
            <button type="submit" class="btn btn-primary justify-content-center" id="load">
                Отправить
            </button>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#message').fadeOut();
        $('#phone').mask("+38 (999) 999-9999");

        $('#feedback').submit(function(event) {
            event.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: new FormData(this),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $('#load').html('<i class="fa fa-circle-o-notch fa-spin"></i> обработка').prop('disabled', true);
                },
                success: function(result) {
                    toggleMessage(result);
                },
                error: function (err) {
                    toggleMessage('Произошла ошибка. Проверьте ввод!', err);
                },
                complete: function () {
                    $('#load').html('Отправить').prop('disabled', false);
                    $('#feedback input, #feedback textarea').val("");
                }
            });
        });
        function toggleMessage(data) {
            $('#message').fadeIn().html(data);
            setTimeout(() => {
                $('#message').fadeOut()
            }, 3000);
        }
    });
</script>
</body>
</html>