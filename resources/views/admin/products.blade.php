@extends('admin.layout')

@section('title', 'Products')

@section('content')

    <div class="card">
        <div class="card-header">
            <h1 class="card-title pt-2">
                Products
                <button id="addUserBtn" type="button" data-toggle="modal" data-target="#addUser">
                    <i class="fas fa-plus-circle"></i>
                </button>
            </h1>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-xl-3 mt-2">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('img/default-product.png') }}"
                             alt="Card image user">
                        <div class="card-body">
                            <h5 class="card-title">Product №1</h5>
                            <p class="card-text">description</p>
                            <a href="#" class="btn btn-primary" data-toggle="modal"
                               data-target="#editUser">Edit</a>
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteUser">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-3 mt-2">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('img/default-product.png') }}"
                             alt="Card image user">
                        <div class="card-body">
                            <h5 class="card-title">Product №2</h5>
                            <p class="card-text">description</p>
                            <a href="#" class="btn btn-primary" data-toggle="modal"
                               data-target="#editUser">Edit</a>
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteUser">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-3 mt-2">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('img/default-product.png') }}"
                             alt="Card image user">
                        <div class="card-body">
                            <h5 class="card-title">Product №3</h5>
                            <p class="card-text">description</p>
                            <a href="#" class="btn btn-primary" data-toggle="modal"
                               data-target="#editUser">Edit</a>
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteUser">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-3 mt-2">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('img/default-product.png') }}"
                             alt="Card image user">
                        <div class="card-body">
                            <h5 class="card-title">Product №4</h5>
                            <p class="card-text">description</p>
                            <a href="#" class="btn btn-primary" data-toggle="modal"
                               data-target="#editUser">Edit</a>
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteUser">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-3 mt-2">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('img/default-product.png') }}"
                             alt="Card image user">
                        <div class="card-body">
                            <h5 class="card-title">Product №5</h5>
                            <p class="card-text">description</p>
                            <a href="#" class="btn btn-primary" data-toggle="modal"
                               data-target="#editUser">Edit</a>
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteUser">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Product Modal -->
    <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="addUserLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addUserLabel">Add new product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" class="form-control" id="inputName" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1">Email address</label>
                            <input type="email" class="form-control" id="inputEmail1" placeholder="Enter email">

                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" class="form-control" id="inputPassword"
                                   aria-describedby="passwordHelp" placeholder="Password">
                            <small id="passwordHelp" class="form-text text-muted">Пароль должен содержать не менее 6
                                символов.
                            </small>
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Product Modal -->
    <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="editUserLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editUserLabel">Edit product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="editName">New name</label>
                            <input type="text" class="form-control" id="editName" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="editEmail">New email address</label>
                            <input type="email" class="form-control" id="editEmail" placeholder="Enter email">

                        </div>
                        <div class="form-group">
                            <label for="editPassword">New password</label>
                            <input type="password" class="form-control" id="editPassword"
                                   aria-describedby="editPasswordHelp" placeholder="Password">
                            <small id="editPasswordHelp" class="form-text text-muted">Пароль должен содержать не менее 6
                                символов.
                            </small>
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Product Modal -->
    <div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="deleteUserLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteUserLabel">Delete product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Are you sure you want to delete the product?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>

@endsection