<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
     <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
         <a class="navbar-brand" href="#">Admin</a>

         @if(Auth::check())
             <button class="navbar-toggler d-md-none d-sm-block" type="button" data-toggle="collapse"
                     data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                     aria-label="Toggle navigation">
                 <span class="navbar-toggler-icon"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                 <ul class="navbar-nav mr-auto d-md-none d-sm-block">
                     <li class="nav-item {{ Request::is('/admin') ? 'active' : '' }}">
                         <a class="nav-link" href="/admin">Dashboard <span class="sr-only">(current)</span></a>
                     </li>
                     <li class="nav-item {{ Request::is('admin/orders') ? 'active' : '' }}">
                         <a class="nav-link" href="/admin/orders">Orders</a>
                     </li>
                     <li class="nav-item {{ Request::is('admin/products') ? 'active' : '' }}">
                         <a class="nav-link" href="/admin/products">Products</a>
                     </li>
                     <li class="nav-item {{ Request::is('admin/users') ? 'active' : '' }}">
                         <a class="nav-link" href="/admin/users">Users</a>
                     </li>
                     <li class="nav-item {{ Request::is('admin/trash') ? 'active' : '' }}">
                         <a class="nav-link" href="/admin/trash">Recycle bin</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                             Logout
                         </a>
                     </li>
                 </ul>
             </div>
             <ul class="navbar-nav ml-auto d-none d-lg-block">
                 <li class="nav-item active">
                     <a class="nav-link disabled"><span data-feather="user" class="mr-2"></span>{{ Auth::user()->name }}
                     </a>
                 </li>
             </ul>
         @endif
     </nav>
    @if(Auth::check())
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                        <ul class="nav flex-column mt-2">
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin') ? 'active' : '' }}" href="/admin">
                                    <span data-feather="home"></span>
                                    Dashboard <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/orders') ? 'active' : '' }} d-flex justify-content-between"
                                   href="/admin/orders">
                                    <p class="mb-0"><span data-feather="file"></span>
                                        Orders</p>
                                    <span class="newOrders">@if(!empty($newOrders)) +{{ $newOrders }}@endif</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/products') ? 'active' : '' }}"
                                   href="/admin/products">
                                    <span data-feather="shopping-cart"></span>
                                    Products
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/users') ? 'active' : '' }}"
                                   href="/admin/users">
                                    <span data-feather="users"></span>
                                    Users
                                </a>
                            </li>
                        </ul>

                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Settings</span>
                            <span data-feather="settings"></span>
                        </h6>
                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/trash') ? 'active' : '' }}"
                                   href="/admin/trash">
                                    <span data-feather="trash-2"></span>
                                    Recycle bin
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <span data-feather="log-out"></span>
                                    Logout
                                </a>
                                <form id="logout-form" class="d-none" action="{{ route('logout') }}" method="POST">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="offset-md-2 col-md-10">
                    @yield('content')
                </div>
            </div>
        </div>
    @else
        @yield('login')
    @endif
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script>
    feather.replace()
</script>
</body>
</html>
