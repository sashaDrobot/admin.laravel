@extends('admin.layout')

@section('title', 'Recycle bin')

@section('content')

    <div class="card">
        <div class="card-header">
            <h1 class="card-title">Recycle bin</h1>
            <p class="card-text">заказов - {{ $deletedOrders->count() }}, товаров - 11</p>
        </div>
        <div class="card-body">
            <h3 class="card-title">
                Orders
            </h3>
            <div class="row">
                @foreach($deletedOrders as $order)
                    <div class="col-md-6 col-lg-4 mt-2">
                        <div class="card">
                            <div class="card-body">
                                <p class="card-text">Заказчик: {{ $order->name }}</p>
                                <p class="card-text">Номер: <a href="tel:{{ $order->phone }}"
                                                               class="contacts-link">{{ $order->phone }}</a></p>
                                <p class="card-text">Email: <a href="mailto:{{ $order->email }}"
                                                               class="contacts-link">{{ $order->email }}</a></p>
                                <p class="card-text">Сообщение: {{ $order->message }} </p>
                                <p class="card-text">Дата: {{ date_format($order->created_at, 'd.m.Y h:m') }}</p>
                                <button type="button" class="btn btn-danger justify-content-end" data-toggle="modal" data-target="#destroyOrder" id="{{ $order->id }}">Delete</button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <h3 class="card-title">
                Products
            </h3>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-xl-3 mt-2">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('img/default-product.png') }}"
                             alt="Card image user">
                        <div class="card-body">
                            <h5 class="card-title">Product №1</h5>
                            <p class="card-text">description</p>
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Destroy Order Modal -->
    <div class="modal fade" id="destroyOrder" tabindex="-1" role="dialog" aria-labelledby="destroyOrderLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="destroyOrderLabel">Delete order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Are you sure you want to delete the order forever?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form id="destroyOrderForm" action="/admin/orders/destroy" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection