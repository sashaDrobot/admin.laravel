/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('order-list-component', require('./components/Orders/OrderListComponent.vue'));
Vue.component('delete-order-component', require('./components/Orders/DeleteOrderComponent.vue'));

const app = new Vue({
    el: '#app',
});

$('#addUser').on('shown.bs.modal', function () {
    $('#inputName').trigger('focus');
    $('#addUserForm').submit(function (event) {
        event.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: new FormData(this),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {

            },
            success: function (res) {
                console.log(res);
                $("#addUser .close").click();
                location.reload();
            },
            error: function (err) {
                let errorMsg = "";
                for (let key in err.responseJSON.errors) {
                    errorMsg += `<p class="mb-0">${err.responseJSON.errors[key]}</p>`;
                }
                $('#addMessage').fadeIn().html(errorMsg);
            },
        });
    });
}).on('hidden.bs.modal', function () {
    $('#addUserForm input').val('');
    $('#addMessage').fadeOut();
});

$('#editUser').on('shown.bs.modal', function (e) {
    $('#editName').trigger('focus');
    $('#editUserForm').attr('action', `/admin/user/edit/${e.relatedTarget.id}`)
        .submit(function (event) {
            event.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: new FormData(this),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {

                },
                success: function (res) {
                    console.log(res);
                    $("#editUser .close").click();
                    location.reload();
                },
                error: function (err) {
                    let errorMsg = "";
                    for (let key in err.responseJSON.errors) {
                        errorMsg += `<p class="mb-0">${err.responseJSON.errors[key]}</p>`;
                    }
                    $('#editMessage').fadeIn().html(errorMsg);
                },
            });
        });
}).on('hidden.bs.modal', function () {
    $('#editUserForm input').val('');
    $('#editMessage').fadeOut();
});

$('#deleteUser').on('shown.bs.modal', function (e) {
    $('#deleteUserForm').attr('action', `/admin/user/delete/${e.relatedTarget.id}`);
});

$('#deleteOrder').on('shown.bs.modal', function (e) {
    // $('#deleteOrderForm').attr('action', `/admin/order/delete/${e.relatedTarget.id}`);
    Vue.set(app.$children[0].$children[0], 'index', e.relatedTarget.id);
    console.log();
});

$('#destroyOrder').on('shown.bs.modal', function (e) {
    $('#destroyOrderForm').attr('action', `/admin/order/destroy/${e.relatedTarget.id}`);
});

$(document).ready(function () {

});
