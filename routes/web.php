<?php

Route::get('/', function () {
    return view('index');
});
Route::post('/', 'OrderController@add');

Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'Auth\LoginController@login');
Route::post('admin/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/admin', 'AdminController@index');
Route::get('/admin/orders', 'AdminController@orders');
Route::get('/admin/products', 'AdminController@products');
Route::get('/admin/users', 'AdminController@users');
Route::get('admin/trash', 'AdminController@trash');

Route::get('/admin/orders/get', 'OrderController@get');
Route::post('/admin/order/accept/{id}', 'OrderController@accept');
Route::post('/admin/order/ready/{id}', 'OrderController@ready');
Route::post('/admin/order/delete/{id}', 'OrderController@delete');
Route::delete('/admin/order/destroy/{id}', 'OrderController@destroy');

Route::post('/admin/users/add', 'UserController@add');
Route::put('/admin/user/edit/{id}', 'UserController@edit');
Route::delete('/admin/user/delete/{id}', 'UserController@delete');