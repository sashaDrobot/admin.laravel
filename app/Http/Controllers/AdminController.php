<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    public function __construct()
    {
        View::share('newOrders', Order::where('status', 'new')->count());
        $this->middleware('auth');
    }

    public function index()
    {
        $orders = Order::get();
        return view('admin.index', compact('orders'));
    }

    public function orders()
    {
        $orders = (object)[
            'new' => Order::where('status', 'pending')->orderBy('created_at', 'desc')->get(),
            'accepted' => Order::where('status', 'development')->orderBy('created_at', 'desc')->get(),
            'completed' => Order::where('status', 'completed')->orderBy('created_at', 'desc')->get()
        ];

        return view('admin.orders', compact('orders'));
    }

    public function products()
    {
        return view('admin.products');
    }

    public function users()
    {
        $users = User::get();
        return view('admin.users', compact('users'));
    }

    public function trash()
    {
        $deletedOrders = Order::where('status', 'deleted')->orderBy('updated_at', 'desc')->get();
        return view('admin.trash', compact('deletedOrders'));
    }
}
