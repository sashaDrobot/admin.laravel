<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json('Success', 200);
    }

    public function edit(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'newname' => 'sometimes|nullable|string|max:255',
            'newemail' => 'sometimes|nullable|string|email|max:255|unique:users,email',
            'newpassword' => 'sometimes|nullable|string|min:6',
        ]);

        if ($request->filled('newname')) {
            $user->name = $request->newname;
        }
        if ($request->filled('newemail')) {
            $user->email = $request->newemail;
        }
        if ($request->filled('newpassword')) {
            $user->password = Hash::make($request->newpassword);
        }

        $user->save();

        return response()->json('Success', 200);
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        if ($user->name !== 'Admin') {
            $user->delete();
        }
        return back();
    }
}
