<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function get()
    {
        $orders = Order::where('status', '!=', 'deleted')->get();
        return $orders;
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'phone' => 'required|max:20',
            'email' => 'required|max:100|email',
            'message' => 'required|max:500'
        ]);

        $order = new Order();

        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->email = $request->email;
        $order->message = $request->message;

        $order->save();

        return response()->json('Success', 200);
    }

    public function accept($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'accepted';
        $order->save();
    }

    public function ready($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'completed';
        $order->save();
    }

    public function delete($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'deleted';
        $order->save();
    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();
//        return back();
    }
}
